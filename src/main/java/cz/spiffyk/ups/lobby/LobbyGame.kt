package cz.spiffyk.ups.lobby

import java.util.*

data class LobbyGame(
        val uuid: UUID,
        var name: String = "",
        var state: State = State.AWAITING_STATE
) {

    enum class State {
        AWAITING_STATE, WAITING, IN_GAME
    }
}