package cz.spiffyk.ups.net

import com.google.common.util.concurrent.SimpleTimeLimiter
import com.google.common.util.concurrent.UncheckedTimeoutException
import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgGarbageException
import cz.spiffyk.ups.net.inmsg.sys.DisconnectMsg
import cz.spiffyk.ups.net.inmsg.sys.LoginAcceptMsg
import cz.spiffyk.ups.net.inmsg.sys.PingMsg
import cz.spiffyk.ups.lobby.LobbyGame
import cz.spiffyk.ups.net.inmsg.game.*
import cz.spiffyk.ups.net.inmsg.lobby.*
import cz.spiffyk.ups.net.inmsg.sys.*
import cz.spiffyk.ups.notifier.*
import cz.spiffyk.ups.notifier.game.*
import cz.spiffyk.ups.notifier.lobby.*
import mu.KotlinLogging
import java.io.*
import java.net.ConnectException
import java.net.Socket
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * High level communication layer.
 */
object PongClient {

    const val MAX_TIMEOUTS = 5
    const val MAX_REINITS = 3

    private val logger = KotlinLogging.logger {}

    private var comms: Comms? = null

    @Volatile
    var lastGameUUID: UUID? = null

    @Volatile
    var lastRejoinKey: Int? = null

    /**
     * Checks whether the client is connected to a server.
     */
    val connected
        get() = (comms !== null && comms!!.connected)

    /**
     * The last retrieved ping latency.
     */
    val latency
        get() = comms?.latency ?: -1


    /**
     * Attempts to connect to a server with the specified parameters.
     */
    fun connect(ip: String, port: Int, nickname: String) {
        try {
            comms?.disconnect(100)
            comms = Comms(ip, port, PongClient::handleInMsg)
            comms?.send("login $nickname")
        } catch (e: ConnectException) {
            Notifier.broadcast(TextNotification(
                    "Could not connect to server!\n${e.message}",
                    "Connection error",
                    TextNotification.Type.ERROR))
            logger.error(e) { "Could not connect to server!" }
            Notifier.broadcast(LogoutNotification)
        } catch (e: Exception) {
            Notifier.broadcast(TextNotification(
                    "An error occurred while connecting to the server!\n${e.message}",
                    "Error",
                    TextNotification.Type.ERROR))
            logger.error(e) { "An error occurred while connecting to the server!" }
            Notifier.broadcast(LogoutNotification)
        }
    }


    /**
     * Gracefully disconnects from the currently connected server (if applicable, otherwise does nothing).
     */
    fun disconnect() {
        try {
            comms?.send("logout")
            comms?.disconnect(5000)
        } catch (e: Exception) {
            logger.error(e) { "An error occurred while disconnecting from the server!" }
        } finally {
            comms = null
        }
    }

    fun joinGame(uuid: UUID) {
        lastGameUUID = uuid
        comms?.send("join $uuid")
    }

    fun rejoinGame(uuid: UUID, rejoinKey: Int) {
        lastGameUUID = uuid
        comms?.send("rejoin $uuid $rejoinKey")
    }

    fun newGame(name: String) {
        comms?.send("lobby_game_create $name")
    }

    fun leaveGame() {
        comms?.send("leave")
    }

    fun paddle(ypos: Double, yvel: Int) {
        comms?.send("paddle $ypos $yvel")
    }


    private fun handleInMsg(msg: InMsg) {
        when (msg) {
            // Sys
            is LoginAcceptMsg -> Notifier.broadcast(LoginNotification(msg.motd))
            is DisconnectMsg -> Notifier.broadcast(LogoutNotification)
            is ErrorMsg -> Notifier.broadcast(TextNotification(
                    msg.message,
                    "Error message from server",
                    TextNotification.Type.ERROR
            ))

            // Lobby
            is LobbyResetMsg -> Notifier.broadcast(LobbyResetNotification)
            is LobbyGameNameMsg -> Notifier.broadcast(LobbyGameNameNotification(msg.uuid, msg.name))
            is LobbyGameStateMsg -> {
                try {
                    Notifier.broadcast(LobbyGameStateNotification(msg.uuid, LobbyGame.State.valueOf(msg.state)))
                } catch (e: IllegalArgumentException) {
                    throw InMsgGarbageException(e)
                }
            }
            is LobbyGameRemoveMsg -> Notifier.broadcast(LobbyGameRemoveNotification(msg.uuid))
            is JoinAcceptMsg -> {
                lastRejoinKey = msg.rejoinKey
                Notifier.broadcast(JoinAcceptNotification(msg.playerNo, msg.rejoinKey))
            }

            // In-game
            is BallMsg -> Notifier.broadcast(BallNotification(msg.xpos, msg.ypos, msg.xvel, msg.yvel))
            is GameFinishedMsg -> Notifier.broadcast(TextNotification("Game over - '${msg.winner}' wins!", "Game over"))
            is GameMetaMsg -> Notifier.broadcast(GameMetaNotification(
                    msg.fieldWidth,
                    msg.fieldHeight,
                    msg.ballSize,
                    msg.paddleWidth,
                    msg.paddleHeight,
                    msg.paddleMargin,
                    msg.winScore))
            is PlayerNameMsg -> Notifier.broadcast(PlayerNameNotification(msg.playerId, msg.name))
            is GameScoresMsg -> Notifier.broadcast(GameScoresNotification(msg.scoreA, msg.scoreB))
            is PaddleMsg -> Notifier.broadcast(PaddleNotification(msg.playerId, msg.ypos, msg.yvel))
            is StartTimeoutMsg -> Notifier.broadcast(StartTimeoutNotification(msg.time))
            is ConnectionErrorMsg -> Notifier.broadcast(ConnectionProblemsNotification(msg.desc))
            is ConnectionRestoredMsg -> Notifier.broadcast(ConnectionRestoredNotification)
        }
    }


    /**
     * Lower-level communication layer.
     */
    private class Comms(val ip: String, val port: Int, val inMsgHandler: (InMsg) -> Unit) {

        private val logger = KotlinLogging.logger {}

        private val worker = InputWorker()
        private val workerThread = Thread(worker)

        @Volatile
        var latency: Int = -1
            private set

        var connected: Boolean = true
            private set

        init {
            logger.info { "Connecting to $ip:$port" }
            workerThread.start()
        }

        /**
         * Sends the specified message to the server.
         */
        fun send(message: String, trace: Boolean = true) {
            if (!connected) {
                throw IllegalStateException("Cannot send a message when not connected")
            }

            if (trace) {
                logger.trace { "Sending message: $message" }
            }
            worker.out!!.println(message)
            worker.out!!.flush()
        }

        /**
         * Closes the communication socket. Does **not** send a logout message beforehand.
         */
        fun disconnect(timeoutMillis: Long = 0) {
            if (!connected) {
                return
            }

            workerThread.join(timeoutMillis)
            if (workerThread.isAlive) {
                workerThread.interrupt()
            }

            worker.sock!!.close()
        }


        /**
         * A worker for reading server input and keeping the connection alive by returning ping.
         */
        inner class InputWorker: Runnable {
            private val logger = KotlinLogging.logger {}

            var sock: Socket? = null
            var out: PrintWriter? = null
            var inps: InputStream? = null
            var inp: BufferedReader? = null

            init {
                reinit()
            }

            @Synchronized
            fun reinit() {
                if (sock !== null && !(sock!!.isClosed)) {
                    sock!!.close()
                }

                sock = Socket(ip, port)
                out = PrintWriter(OutputStreamWriter(sock!!.getOutputStream()))
                inps = sock!!.getInputStream()
                inp = BufferedReader(InputStreamReader(inps))
            }

            override fun run() {
                val timeLimiter = SimpleTimeLimiter()
                var gracefulLogout = false
                var timeouts = 0

				try {
					while (!gracefulLogout) {
						try {
							val line = timeLimiter.callWithTimeout(inp!!::readLine, 3L, TimeUnit.SECONDS, true)
							if (timeouts > 0) {
								Notifier.broadcast(ConnectionRestoredNotification)
								timeouts = 0
							}
							if (line === null || line.isEmpty()) {
								continue;
							}
							val msg = InMsg.fromLine(line)

							if (msg !is PingMsg) { // not logging ping because that would be... huge...
								logger.trace { "Server message: $line" }
							}

							// Handle low-level stuff, like ping returns etc.
							when (msg) {
								is PingMsg -> {
									send("pong ${msg.uuid}", false) // not logging pong for the same reasons
									latency = msg.latency
								}
								is DisconnectMsg -> gracefulLogout = true
							}

							inMsgHandler(msg) // handle high-level stuff
						} catch (e: InMsgGarbageException) {
							logger.error(e) { "Server has sent garbage" }
						} catch (e: IOException) {
							logger.error(e) { "I/O Exception" }
						} catch (e: UncheckedTimeoutException) {
							logger.error { "Communication timing out!" }
							Notifier.broadcast(ConnectionProblemsNotification("Connection timing out!"))
							timeouts++
							if (timeouts > MAX_TIMEOUTS) {
								break
							}
						}
					}

					if (!gracefulLogout) {
						sock!!.close()
						sock = null
						out = null
						inps = null
						inp = null
						Notifier.broadcast(ConnectionErrorNotification)
					}
				} finally {
					sock?.close()
				}

                connected = false
            }
        }

    }

}
