package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class PlayerNameMsg(val playerId: Int, val name: String) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.PLAYER_NAME

    constructor(lp: InMsgLineParser) : this(lp.integer().toInt(), lp.stringRest())
}