package cz.spiffyk.ups.net.inmsg

import java.lang.IllegalArgumentException
import java.util.*

class InMsgLineParser(private val line: String) {

    val messageName: String

    private var spaceIndex = line.indexOf(' ')

    init {
        messageName = if (spaceIndex == -1) {
            line
        } else {
            line.substring(0, spaceIndex)
        }
    }

    fun stringWordOptional(): String? {
        return if (spaceIndex == -1) {
            null
        } else {
            val lastIndex = spaceIndex + 1
            spaceIndex = line.indexOf(' ', lastIndex)
            if (spaceIndex == -1) {
                line.substring(lastIndex)
            } else {
                line.substring(lastIndex, spaceIndex)
            }
        }
    }

    fun stringWord(): String = stringWordOptional() ?: throw InMsgGarbageException()

    fun stringRest(): String {
        return if (spaceIndex == -1) {
            ""
        } else {
            val lastIndex = spaceIndex + 1
            spaceIndex = -1
            line.substring(lastIndex)
        }
    }

    fun uuidOptional(): UUID? {
        val word = stringWordOptional()
        return if (word === null) {
            null
        } else {
            try {
                UUID.fromString(word)
            } catch (e: IllegalArgumentException) {
                throw InMsgGarbageException(e)
            }
        }
    }

    fun uuid(): UUID = uuidOptional() ?: throw InMsgGarbageException()

    fun integerOptional(): Long? {
        val word = stringWordOptional()
        return if (word === null) {
            null
        } else {
            try {
                word.toLong()
            } catch (e: NumberFormatException) {
                throw InMsgGarbageException(e)
            }
        }
    }

    fun integer(): Long = integerOptional() ?: throw InMsgGarbageException()

    fun decimalOptional(): Double? {
        val word = stringWordOptional()
        return if (word === null) {
            null
        } else {
            try {
                word.toDouble()
            } catch (e: NumberFormatException) {
                throw InMsgGarbageException(e)
            }
        }
    }

    fun decimal(): Double = decimalOptional() ?: throw InMsgGarbageException()

}