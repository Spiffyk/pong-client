package cz.spiffyk.ups.net.inmsg.lobby

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser
import java.util.*

data class LobbyGameStateMsg(val uuid: UUID, val state: String) : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.LOBBY_GAME_STATE

    constructor(lp: InMsgLineParser) : this(lp.uuid(), lp.stringWord())
}