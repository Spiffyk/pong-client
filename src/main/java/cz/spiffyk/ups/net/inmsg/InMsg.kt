package cz.spiffyk.ups.net.inmsg

import cz.spiffyk.ups.net.inmsg.game.*
import cz.spiffyk.ups.net.inmsg.lobby.*
import cz.spiffyk.ups.net.inmsg.sys.DisconnectMsg
import cz.spiffyk.ups.net.inmsg.sys.ErrorMsg
import cz.spiffyk.ups.net.inmsg.sys.LoginAcceptMsg
import cz.spiffyk.ups.net.inmsg.sys.PingMsg

interface InMsg {

    companion object {

        @JvmStatic
        fun fromLine(line: String): InMsg {
            val lp = InMsgLineParser(line)
            return (Type.map[lp.messageName] ?: throw InMsgGarbageException()).parserFunc(lp)
        }

    }

    /**
     * The type of the input message from the server.
     */
    val type: InMsg.Type


    enum class Type(
            val messageName: String,
            val parserFunc: (InMsgLineParser) -> (InMsg)
    ) {
        PING("ping", ::PingMsg),
        LOGIN_ACCEPT("login_accept", ::LoginAcceptMsg),
        DISCONNECT("disconnect", ::DisconnectMsg),
        ERROR("error", ::ErrorMsg),

        LOBBY_RESET("lobby_reset", { LobbyResetMsg }),
        LOBBY_GAME_NAME("lobby_game_name", ::LobbyGameNameMsg),
        LOBBY_GAME_STATE("lobby_game_state", ::LobbyGameStateMsg),
        LOBBY_GAME_REMOVE("lobby_game_remove", ::LobbyGameRemoveMsg),
        JOIN_ACCEPT("join_accept", ::JoinAcceptMsg),

        GAME_META("game_meta", ::GameMetaMsg),
        PLAYER_NAME("player_name", ::PlayerNameMsg),
        GAME_SCORES("game_scores", ::GameScoresMsg),
        START_TIMEOUT("start_timeout", ::StartTimeoutMsg),
        BALL("ball", ::BallMsg),
        PADDLE("paddle", ::PaddleMsg),
        GAME_FINISHED("game_finished", ::GameFinishedMsg),
        CONNECTION_ERROR("connection_error", ::ConnectionErrorMsg),
        CONNECTION_RESTORED("connectionRestored", { ConnectionRestoredMsg });

        companion object {
            val map = Type.values().associateBy(Type::messageName)
        }
    }

}