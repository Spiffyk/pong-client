package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class GameMetaMsg(
        val fieldWidth: Double,
        val fieldHeight: Double,
        val ballSize: Double,
        val paddleWidth: Double,
        val paddleHeight: Double,
        val paddleMargin: Double,
        val winScore: Int
) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.GAME_META

    constructor(lp: InMsgLineParser) : this(
            lp.decimal(),
            lp.decimal(),
            lp.decimal(),
            lp.decimal(),
            lp.decimal(),
            lp.decimal(),
            lp.integer().toInt())
}