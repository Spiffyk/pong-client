package cz.spiffyk.ups.net.inmsg.lobby

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser
import java.util.*

data class LobbyGameNameMsg(val uuid: UUID, val name: String) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.LOBBY_GAME_NAME

    constructor(lp: InMsgLineParser) : this(lp.uuid(), lp.stringRest())
}