package cz.spiffyk.ups.net.inmsg.sys

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class DisconnectMsg(val reason: String) : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.DISCONNECT

    constructor(lp: InMsgLineParser) : this(lp.stringRest())
}