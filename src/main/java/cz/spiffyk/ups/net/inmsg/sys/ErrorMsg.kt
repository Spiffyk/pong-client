package cz.spiffyk.ups.net.inmsg.sys

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class ErrorMsg(val message: String) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.ERROR

    constructor(lp: InMsgLineParser) : this(lp.stringRest())
}