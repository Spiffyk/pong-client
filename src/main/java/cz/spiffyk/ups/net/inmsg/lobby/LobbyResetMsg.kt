package cz.spiffyk.ups.net.inmsg.lobby

import cz.spiffyk.ups.net.inmsg.InMsg

object LobbyResetMsg : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.LOBBY_RESET
}