package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class GameScoresMsg(val scoreA: Int, val scoreB: Int) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.GAME_SCORES

    constructor(lp: InMsgLineParser) : this(lp.integer().toInt(), lp.integer().toInt())
}