package cz.spiffyk.ups.net.inmsg.lobby

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser
import java.util.*

data class LobbyGameRemoveMsg(val uuid: UUID) : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.LOBBY_GAME_REMOVE

    constructor(lp: InMsgLineParser) : this(lp.uuid())
}