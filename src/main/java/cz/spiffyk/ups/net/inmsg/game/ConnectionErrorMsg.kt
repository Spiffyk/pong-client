package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class ConnectionErrorMsg(val desc: String) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.CONNECTION_ERROR

    constructor(lp: InMsgLineParser) : this(lp.stringRest())
}