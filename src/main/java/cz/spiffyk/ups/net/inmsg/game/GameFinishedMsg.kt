package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class GameFinishedMsg(val winner: String) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.GAME_FINISHED

    constructor(lp: InMsgLineParser) : this(lp.stringRest())
}