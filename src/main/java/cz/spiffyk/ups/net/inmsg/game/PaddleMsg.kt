package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class PaddleMsg(
        val playerId: Int,
        val ypos: Double,
        val yvel: Double
) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.PADDLE

    constructor(lp: InMsgLineParser) : this(lp.integer().toInt(), lp.decimal(), lp.decimal())
}