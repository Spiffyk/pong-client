package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class StartTimeoutMsg(val time: Int) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.START_TIMEOUT

    constructor(lp: InMsgLineParser) : this(lp.integer().toInt())
}