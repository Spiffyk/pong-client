package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class BallMsg(
        val xpos: Double,
        val ypos: Double,
        val xvel: Double,
        val yvel: Double
) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.BALL

    constructor(lp: InMsgLineParser) : this(
            lp.decimal(),
            lp.decimal(),
            lp.decimal(),
            lp.decimal())
}