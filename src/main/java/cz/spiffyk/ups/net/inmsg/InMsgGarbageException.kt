package cz.spiffyk.ups.net.inmsg

class InMsgGarbageException(cause: Throwable? = null) : Exception(cause)