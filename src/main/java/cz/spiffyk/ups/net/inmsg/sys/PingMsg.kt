package cz.spiffyk.ups.net.inmsg.sys

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser
import java.util.*

data class PingMsg(val uuid: UUID, val latency: Int) : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.PING

    constructor(lp: InMsgLineParser) : this(lp.uuid(), lp.integerOptional()?.toInt() ?: -1)

}