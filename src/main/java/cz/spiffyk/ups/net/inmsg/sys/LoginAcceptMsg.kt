package cz.spiffyk.ups.net.inmsg.sys

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class LoginAcceptMsg(val motd: String) : InMsg {

    override val type: InMsg.Type
        get() = InMsg.Type.LOGIN_ACCEPT


    constructor(lp: InMsgLineParser) : this(lp.stringRest())

}