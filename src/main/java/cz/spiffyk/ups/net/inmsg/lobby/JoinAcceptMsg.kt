package cz.spiffyk.ups.net.inmsg.lobby

import cz.spiffyk.ups.net.inmsg.InMsg
import cz.spiffyk.ups.net.inmsg.InMsgLineParser

data class JoinAcceptMsg(val playerNo: Int, val rejoinKey: Int) : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.JOIN_ACCEPT

    constructor(lp: InMsgLineParser) : this(lp.integer().toInt(), lp.integer().toInt())
}