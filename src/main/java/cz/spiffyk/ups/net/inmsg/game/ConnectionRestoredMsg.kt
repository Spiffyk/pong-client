package cz.spiffyk.ups.net.inmsg.game

import cz.spiffyk.ups.net.inmsg.InMsg

object ConnectionRestoredMsg : InMsg {
    override val type: InMsg.Type
        get() = InMsg.Type.CONNECTION_RESTORED
}