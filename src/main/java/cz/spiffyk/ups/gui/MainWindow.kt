package cz.spiffyk.ups.gui

import cz.spiffyk.ups.net.PongClient
import cz.spiffyk.ups.notifier.*
import cz.spiffyk.ups.notifier.lobby.JoinAcceptNotification
import cz.spiffyk.ups.notifier.lobby.LobbyResetNotification
import java.awt.CardLayout
import java.awt.Dimension
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JFrame
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.Timer
import kotlin.system.exitProcess

class MainWindow : JFrame("Pong Client") {

    private val messages = Notifier.queue()
    private val mqCheckTimer = Timer(100) { checkMessageQueue() }

    companion object {
        const val LOGIN_SCREEN_ID = "LoginScreen"
        const val LOBBY_SCREEN_ID = "LobbyScreen"
        const val GAME_SCREEN_ID = "GameScreen"
    }

    private val loginScreen = LoginScreen()
    private val lobbyScreen = LobbyScreen()
    private val gameScreen = GameScreen()

    private val cl
        get() = (contentPane.layout as CardLayout)

    init {
        // initialize swappable screens
        contentPane = JPanel(CardLayout())
        contentPane.add(lobbyScreen, LOBBY_SCREEN_ID)
        contentPane.add(loginScreen, LOGIN_SCREEN_ID)
        contentPane.add(gameScreen, GAME_SCREEN_ID)
        cl.show(contentPane, LOGIN_SCREEN_ID)

        // initialize window properties
        minimumSize = Dimension(640, 480)
        size = Dimension(800, 600)
        setLocationRelativeTo(null)
        isResizable = true
        defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE

        // utilities
        mqCheckTimer.start()
        addWindowListener(MainWindowListener())
    }

    private fun checkMessageQueue() {
        this.title = if (PongClient.connected && PongClient.latency >= 0) {
            "Pong Client (ping: ${PongClient.latency})"
        } else {
            "Pong Client"
        }

        while (messages.isNotEmpty()) {
            val message = messages.poll()
            when (message) {
                is TextNotification -> {
                    JOptionPane.showMessageDialog(
                            this, message.text, message.title ?: "", message.type.swingType)
                }
                is LobbyResetNotification -> {
                    lobbyScreen.setControlEnabled(true)
                    cl.show(contentPane, LOBBY_SCREEN_ID)
                    lobbyScreen.requestFocusInWindow()
                }
                is LogoutNotification -> {
                    loginScreen.setControlEnabled(true)
                    cl.show(contentPane, LOGIN_SCREEN_ID)
                    loginScreen.requestFocusInWindow()
                }
                is JoinAcceptNotification -> {
                    cl.show(contentPane, GAME_SCREEN_ID)
                    gameScreen.requestFocusInWindow()
                }
                is ConnectionErrorNotification -> {
                    Notifier.broadcast(LogoutNotification)
                    Notifier.broadcast(TextNotification("Connection error!", "Error", TextNotification.Type.ERROR))
                }
            }
        }
        mqCheckTimer.restart()
    }

    inner class MainWindowListener : WindowAdapter() {
        override fun windowClosing(e: WindowEvent?) {
            val result = JOptionPane.showConfirmDialog(
                    this@MainWindow,
                    "Are you sure you want to quit?",
                    "Exit",
                    JOptionPane.YES_NO_OPTION)

            if (result != JOptionPane.YES_OPTION) {
                return
            }

            this@MainWindow.isVisible = false
            PongClient.disconnect()
            exitProcess(0)
        }
    }

}