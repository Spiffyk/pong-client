package cz.spiffyk.ups.gui

import cz.spiffyk.ups.net.PongClient
import cz.spiffyk.ups.notifier.ConnectionProblemsNotification
import cz.spiffyk.ups.notifier.ConnectionRestoredNotification
import cz.spiffyk.ups.notifier.Notifier
import cz.spiffyk.ups.notifier.game.*
import cz.spiffyk.ups.notifier.lobby.*
import java.awt.*
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.geom.AffineTransform
import java.awt.geom.Rectangle2D
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.Timer
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.math.sqrt

class GameScreen : JPanel(CardLayout()) {

    companion object {
        private const val MAIN_GAME_SCREEN = "MainGameScreen"
        private const val WAITING_SCREEN = "WaitingScreen"
    }

    private val keyListener = object : KeyAdapter() {
        override fun keyPressed(e: KeyEvent) {
            when(e.keyCode) {
                KeyEvent.VK_ESCAPE -> { PongClient.leaveGame() }
                KeyEvent.VK_UP -> { direction = -1 }
                KeyEvent.VK_DOWN -> { direction = 1 }
            }
        }

        override fun keyReleased(e: KeyEvent) {
            when(e.keyCode) {
                KeyEvent.VK_ESCAPE -> { PongClient.leaveGame() }
                KeyEvent.VK_UP, KeyEvent.VK_DOWN -> { direction = 0 }
            }
        }
    }

    private val componentListener = object : ComponentAdapter() {
        override fun componentShown(e: ComponentEvent?) {
			connectionProblem = null
            ts = System.currentTimeMillis()
            cl.show(this@GameScreen, WAITING_SCREEN)
            statusLabel.text = "Running..."
        }
    }

    var lastTs = 0L
    var ts = 0L
    var paddleWidth: Double = 3.0
    var paddleHeight: Double = 11.0
    var paddleMargin: Double = 5.0
    var ballSize: Double = 3.0
    var fieldWidth: Double = 100.0
    var fieldHeight: Double = 60.0

    var startTimeout: Int = 0
    var playerId: Int = 0
    val playerPaddle: Paddle
        get() = paddle(playerId)

    var direction = 0
    var lastDirection = 0

    var connectionProblem: String? = null

    val pa = Paddle()
    val pb = Paddle()
    val b = Ball()

    val timeoutFont: Font

    val cl
        get() = (layout as CardLayout)

    val mainGameScreen = JPanel(BorderLayout())
    val waitingForPlayers = JLabel("Waiting for a second player", JLabel.CENTER)

    val topPanel = JPanel(FlowLayout(FlowLayout.CENTER, 8, 5))

    val scoreALabel = JLabel("N/A", JLabel.RIGHT)
    val nameALabel = JLabel("-", JLabel.RIGHT)
    val nameBLabel = JLabel("-", JLabel.LEFT)
    val scoreBLabel = JLabel("N/A", JLabel.LEFT)
    val gameSurface = GameSurface()
    val statusLabel = JLabel("", JLabel.CENTER)

    init {
        isFocusable = true
        addKeyListener(keyListener)
        addComponentListener(componentListener)

        timeoutFont = Font.createFont(
                Font.TRUETYPE_FONT, javaClass.classLoader.getResourceAsStream("OpenSans-Bold.ttf"))

        topPanel.add(scoreALabel)
        topPanel.add(nameALabel)
        topPanel.add(JLabel(":", JLabel.CENTER))
        topPanel.add(nameBLabel)
        topPanel.add(scoreBLabel)

        mainGameScreen.add(topPanel, BorderLayout.NORTH)
        mainGameScreen.add(gameSurface, BorderLayout.CENTER)
        mainGameScreen.add(statusLabel, BorderLayout.SOUTH)

        add(mainGameScreen, MAIN_GAME_SCREEN)
        add(waitingForPlayers, WAITING_SCREEN)
    }

    private fun paddle(id: Int) = if (id == 0) pa else pb

    inner class GameSurface : JPanel() {

        private val messages = Notifier.queue()
        private val mqCheckTimer = Timer(1000) { checkMessageQueue() }

        init {
            mqCheckTimer.start()
        }

        private fun checkMessageQueue() {
            while (messages.isNotEmpty()) {
                val message = messages.poll()
                when (message) {
                    is JoinAcceptNotification -> {
						connectionProblem = null
                        playerId = message.playerNo
                        pa.ypos = 0.0
                        pb.ypos = 0.0
                        b.xpos = 0.0
                        b.ypos = 0.0
                    }
                    is GameMetaNotification -> {
						connectionProblem = null
                        fieldWidth = message.fieldWidth
                        fieldHeight = message.fieldHeight
                        ballSize = message.ballSize
                        paddleWidth = message.paddleWidth
                        paddleHeight = message.paddleHeight
                        paddleMargin = message.paddleMargin
                    }
                    is BallNotification -> {
						connectionProblem = null
                        b.xpos = message.xpos
                        b.ypos = message.ypos
                        b.xvel = message.xvel
                        b.yvel = message.yvel
                    }
                    is PaddleNotification -> {
						connectionProblem = null
                        val paddle = paddle(message.playerId)
                        paddle.ypos = message.ypos
                        paddle.yvel = message.yvel
                    }
                    is PlayerNameNotification -> {
						connectionProblem = null
                        if (message.playerId == 0) {
                            nameALabel.text = message.name
                        } else {
                            nameBLabel.text = message.name
                        }
                    }
                    is GameScoresNotification -> {
						connectionProblem = null
                        scoreALabel.text = "${message.scoreA}"
                        scoreBLabel.text = "${message.scoreB}"
                    }
                    is StartTimeoutNotification -> {
						connectionProblem = null
                        startTimeout = message.time
                        cl.show(this@GameScreen, MAIN_GAME_SCREEN)
                    }
                    is ConnectionProblemsNotification -> {
                        connectionProblem = message.desc
                    }
                    is ConnectionRestoredNotification -> {
                        connectionProblem = null
                    }
                }
            }
            if (connectionProblem === null) {
                statusLabel.text = "Running"
            } else {
                statusLabel.text = "Connection problem: $connectionProblem"
            }
            mqCheckTimer.restart()
        }

        fun update(delta: Double) {
            if (direction != lastDirection) {
                PongClient.paddle(playerPaddle.ypos, direction)
                lastDirection = direction
            }

            if (startTimeout > 0) {
                return
            }

            if (connectionProblem !== null) {
                return
            }

            // Process movement
            pa.ypos += pa.yvel * delta
            pb.ypos += pb.yvel * delta
            b.xpos += b.xvel * delta
            b.ypos += b.yvel * delta
        }

        override fun paint(gOrig: Graphics) {
            super.paint(gOrig)

            checkMessageQueue()

            val rect = Rectangle2D.Double()

            lastTs = ts
            ts = System.currentTimeMillis()
            update((ts - lastTs) / 1000.0)

            val g = gOrig as Graphics2D

            g.transform = AffineTransform()

            val scale = min(size.width / fieldWidth, size.height / fieldHeight)
            g.translate((size.width / 2) - ((scale * fieldWidth) / 2), (size.height / 2) - ((scale * fieldHeight) / 2))
            g.scale(scale, scale)
            g.translate(fieldWidth / 2, fieldHeight / 2)

            g.color = Color.BLACK
            rect.x = - fieldWidth / 2
            rect.y = - fieldHeight / 2
            rect.width = fieldWidth
            rect.height = fieldHeight
            g.fill(rect)

            g.color = Color.WHITE

            rect.x = paddleMargin - (fieldWidth / 2)
            rect.y = pa.ypos - (paddleHeight / 2)
            rect.width = paddleWidth
            rect.height = paddleHeight
            g.fill(rect)

            rect.x = (fieldWidth / 2) - (paddleMargin + paddleWidth)
            rect.y = pb.ypos - (paddleHeight / 2)
            rect.width = paddleWidth
            rect.height = paddleHeight
            g.fill(rect)

            rect.x = (b.xpos - (ballSize / 2))
            rect.y = (b.ypos - (ballSize / 2))
            rect.width = ballSize
            rect.height = ballSize
            g.fill(rect)

            if (startTimeout > 0) {
                g.color = Color.RED
                val tostr = startTimeout.toString()
                g.font = timeoutFont.deriveFont((fieldHeight / 2).toFloat())

                val bounds = g.font.getStringBounds(tostr, g.fontRenderContext)
                g.drawString(tostr, (- bounds.width / 2).roundToInt(), (bounds.height / 2).roundToInt())
            }

            if (isVisible) {
                Thread.sleep(10)
                repaint()
            }
        }
    }

    data class Paddle(var ypos: Double = 0.0, var yvel: Double = 0.0)

    data class Ball(var xpos: Double = 0.0,
                    var ypos: Double = 0.0,
                    var xvel: Double = 0.0,
                    var yvel: Double = 0.0)

}
