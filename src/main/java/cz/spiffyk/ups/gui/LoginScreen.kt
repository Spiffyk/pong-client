package cz.spiffyk.ups.gui

import cz.spiffyk.ups.net.PongClient
import java.awt.*
import java.awt.event.*
import javax.swing.*
import javax.swing.border.BevelBorder
import javax.swing.border.CompoundBorder

class LoginScreen : JPanel() {

    val container = JPanel()

    val nicknamePanel = JPanel()
    val nicknameField = JTextField("Anonymous")

    val serverPanel = JPanel()
    val ipField = JTextField("127.0.0.1")
    val portField = JTextField("2333")

    val loginPanel = JPanel()
    val loginButton = JButton("Connect")

    private val componentListener = object : ComponentAdapter() {
        override fun componentShown(e: ComponentEvent?) {
            rootPane?.defaultButton = loginButton
        }
    }

    init {
        addComponentListener(componentListener)

        nicknamePanel.layout = BoxLayout(nicknamePanel, BoxLayout.X_AXIS)
        nicknamePanel.add(JLabel("Username:", JLabel.RIGHT))
        nicknamePanel.add(Box.createRigidArea(Dimension(5, 0)))
        nicknamePanel.add(nicknameField)

        serverPanel.layout = BoxLayout(serverPanel, BoxLayout.X_AXIS)
        serverPanel.add(JLabel("Server:", JLabel.RIGHT))
        serverPanel.add(Box.createRigidArea(Dimension(5, 0)))
        serverPanel.add(ipField)
        serverPanel.add(Box.createRigidArea(Dimension(5, 0)))
        serverPanel.add(portField)

        loginButton.addActionListener { loginAction() }
        loginPanel.layout = BorderLayout()
        loginPanel.add(loginButton)

        container.layout = BoxLayout(container, BoxLayout.Y_AXIS)
        container.alignmentX = Component.CENTER_ALIGNMENT
        container.alignmentY = Component.CENTER_ALIGNMENT
        container.preferredSize = Dimension(400, 150)
        container.minimumSize = container.preferredSize
        container.border = CompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.RAISED),
                BorderFactory.createEmptyBorder(20, 20, 20, 20))
        container.add(nicknamePanel)
        container.add(Box.createRigidArea(Dimension(0, 10)))
        container.add(serverPanel)
        container.add(Box.createRigidArea(Dimension(0, 10)))
        container.add(loginPanel)

        minimumSize = container.minimumSize
        alignmentX = Component.CENTER_ALIGNMENT
        alignmentY = Component.CENTER_ALIGNMENT
        layout = GridBagLayout()
        background = Color(128, 128, 128)

        add(container)
    }

    fun setControlEnabled(b: Boolean) {
        nicknameField.isEnabled = b
        ipField.isEnabled = b
        portField.isEnabled = b
        loginButton.isEnabled = b
    }

    private fun loginAction() {
        setControlEnabled(false)
        PongClient.connect(ipField.text, portField.text.toInt(), nicknameField.text)
    }


}