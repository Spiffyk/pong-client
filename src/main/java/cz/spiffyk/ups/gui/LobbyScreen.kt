package cz.spiffyk.ups.gui

import cz.spiffyk.ups.net.PongClient
import cz.spiffyk.ups.lobby.LobbyGame
import cz.spiffyk.ups.notifier.LoginNotification
import cz.spiffyk.ups.notifier.lobby.LobbyResetNotification
import cz.spiffyk.ups.notifier.Notifier
import cz.spiffyk.ups.notifier.lobby.LobbyGameNameNotification
import cz.spiffyk.ups.notifier.lobby.LobbyGameRemoveNotification
import cz.spiffyk.ups.notifier.lobby.LobbyGameStateNotification
import mu.KotlinLogging
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Component
import java.awt.event.*
import java.util.*
import javax.swing.*
import javax.swing.Timer
import kotlin.collections.HashMap

class LobbyScreen : JPanel(BorderLayout()) {

    private val logger = KotlinLogging.logger {}

    private val messages = Notifier.queue()
    private val mqCheckTimer = Timer(100) { checkMessageQueue() }

    val serverMotd = JLabel("", JLabel.CENTER)

    private val gameListModel = GameListModel()
    val gameList = JList(gameListModel)

//    val bottomPanel = JPanel(FlowLayout(FlowLayout.RIGHT))
    val bottomPanel = JToolBar(JToolBar.HORIZONTAL)
    val newGameNameField = JTextField("", 8)
    val newGameButton = JButton("New game")
    val joinButton = JButton("Join")
    val logoutButton = JButton("Log out")

    private val keyListener = object : KeyAdapter() {
        override fun keyPressed(e: KeyEvent?) {
            when(e?.keyCode) {
                KeyEvent.VK_ESCAPE -> {
                    logoutAction()
                }
            }
        }
    }

    private val componentListener = object : ComponentAdapter() {
        override fun componentShown(e: ComponentEvent?) {
            rootPane?.defaultButton = joinButton
        }
    }

    init {
        isFocusable = true
        addKeyListener(keyListener)
        addComponentListener(componentListener)

        serverMotd.border = BorderFactory.createEmptyBorder(8, 8, 8, 8)

        gameList.cellRenderer = GameCellRenderer
        gameList.border = BorderFactory.createLoweredBevelBorder()
        gameList.selectionMode = ListSelectionModel.SINGLE_SELECTION
        gameList.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent?) {
                if ((e?.clickCount ?: 0) >= 2) {
                    joinAction()
                }
            }
        })
        gameList.addFocusListener(object : FocusAdapter() {
            override fun focusGained(e: FocusEvent?) {
                rootPane?.defaultButton = joinButton
            }
        })

        logoutButton.addActionListener { logoutAction() }
        newGameNameField.addFocusListener(object : FocusAdapter() {
            override fun focusGained(e: FocusEvent?) {
                rootPane?.defaultButton = newGameButton
            }
        })
        newGameButton.addActionListener { newGameAction() }
        joinButton.addActionListener { joinAction() }

        bottomPanel.isFloatable = false
        bottomPanel.add(logoutButton)
        bottomPanel.addSeparator()
        bottomPanel.add(JLabel("Game name:"))
        bottomPanel.add(newGameNameField)
        bottomPanel.add(newGameButton)
        bottomPanel.addSeparator()
        bottomPanel.add(joinButton)

        add(serverMotd, BorderLayout.NORTH)
        add(gameList, BorderLayout.CENTER)
        add(bottomPanel, BorderLayout.SOUTH)

        border = BorderFactory.createEmptyBorder(2, 2, 2, 2)
        mqCheckTimer.start()
    }

    fun setControlEnabled(b: Boolean) {
        gameList.isEnabled = b
        logoutButton.isEnabled = b
        newGameNameField.isEnabled = b
        newGameButton.isEnabled = b
        joinButton.isEnabled = b
    }

    private fun checkMessageQueue() {
        while (messages.isNotEmpty()) {
            val message = messages.poll()
            when (message) {
                is LobbyResetNotification -> {
                    logger.info { "Resetting lobby" }
                    gameListModel.clear()
                }
                is LobbyGameNameNotification -> {
                    gameListModel.setName(message.uuid, message.name)
                }
                is LobbyGameStateNotification -> {
                    gameListModel.setState(message.uuid, message.state)
                }
                is LobbyGameRemoveNotification -> {
                    gameListModel.remove(message.uuid)
                }
                is LoginNotification -> {
                    serverMotd.text = message.motd
                }
            }
        }

        mqCheckTimer.restart()
    }

    private fun joinAction() {
        val game = gameList.selectedValue
        if (game !== null) {
            if (PongClient.lastGameUUID !== null && PongClient.lastRejoinKey !== null) {
                if (game.uuid == PongClient.lastGameUUID) {
                    PongClient.rejoinGame(PongClient.lastGameUUID!!, PongClient.lastRejoinKey!!)
                    return
                }
            }

            if (game.state !== LobbyGame.State.WAITING) {
                JOptionPane.showMessageDialog(
                        this,
                        "Running games cannot be joined, unless we have a rejoin key!",
                        "Invalid game state",
                        JOptionPane.ERROR_MESSAGE)
                return
            }

            setControlEnabled(false)
            PongClient.joinGame(game.uuid)
        }
    }

    private fun newGameAction() {
        setControlEnabled(false)
        PongClient.newGame(newGameNameField.text)
    }

    private fun logoutAction() {
        setControlEnabled(false)
        val retval = JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to log out?",
                null,
                JOptionPane.YES_NO_OPTION)

        if (retval == JOptionPane.YES_OPTION) {
            PongClient.disconnect()
        }
    }


    private object GameCellRenderer : ListCellRenderer<LobbyGame> {

        val labels = WeakHashMap<LobbyGame, JLabel>()

        override fun getListCellRendererComponent(
                list: JList<out LobbyGame>?,
                value: LobbyGame?,
                index: Int,
                isSelected: Boolean,
                cellHasFocus: Boolean): Component {

            val label = labels.computeIfAbsent(value) {
                val label = JLabel()
                label.border = BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color(0, 0, 0, 32), 1),
                        BorderFactory.createEmptyBorder(3, 5, 3, 5))

                label
            }

            label.text = "${value?.name}"
            if (value?.state == LobbyGame.State.IN_GAME) {
                label.text += " (in-game)"
                label.isEnabled = false
            } else {
                label.isEnabled = true
            }
            if (isSelected) {
                label.isOpaque = true
                label.background = list?.selectionBackground
            } else {
                label.isOpaque = false
                label.background = null
            }
            return label
        }
    }

    private class GameListModel : AbstractListModel<LobbyGame>() {

        private val games = HashMap<UUID, LobbyGame>()
        private var gameList = Collections.emptyList<LobbyGame>()

        override fun getElementAt(index: Int): LobbyGame = gameList[index]

        override fun getSize(): Int = games.size

        fun setName(uuid: UUID, name: String) {
            val game = games.computeIfAbsent(uuid) { LobbyGame(uuid) }
            game.name = name
            update()
        }

        fun setState(uuid: UUID, state: LobbyGame.State) {
            val game = games.computeIfAbsent(uuid) { LobbyGame(uuid) }
            game.state = state
            update()
        }

        fun add(game: LobbyGame) {
            games[game.uuid] = game
            update()
        }

        fun remove(uuid: UUID) {
            games.remove(uuid)
            update()
        }

        fun remove(game: LobbyGame) = remove(game.uuid)

        fun clear() {
            games.clear()
            update()
        }

        private fun update() {
            gameList = games.values.sortedWith(Comparator.comparing(LobbyGame::state).thenComparing(LobbyGame::name))
            fireContentsChanged(this, 0, size)
        }
    }

}