package cz.spiffyk.ups

import cz.spiffyk.ups.gui.MainWindow

fun main(args: Array<String>) {
    val mw = MainWindow()
    mw.isVisible = true
}
