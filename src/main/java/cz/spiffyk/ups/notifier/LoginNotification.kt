package cz.spiffyk.ups.notifier

data class LoginNotification(val motd: String) : Notifier.Notification