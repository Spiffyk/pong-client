package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class StartTimeoutNotification(val time: Int) : Notifier.Notification