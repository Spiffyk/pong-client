package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class BallNotification(
        val xpos: Double,
        val ypos: Double,
        val xvel: Double,
        val yvel: Double
) : Notifier.Notification