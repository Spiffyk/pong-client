package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class GameScoresNotification(val scoreA: Int, val scoreB: Int) : Notifier.Notification