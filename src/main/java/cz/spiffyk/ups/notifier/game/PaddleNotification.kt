package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class PaddleNotification(
        val playerId: Int,
        val ypos: Double,
        val yvel: Double
) : Notifier.Notification