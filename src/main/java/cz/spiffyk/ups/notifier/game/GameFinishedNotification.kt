package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class GameFinishedNotification(val winner: String) : Notifier.Notification