package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class GameMetaNotification(
        val fieldWidth: Double,
        val fieldHeight: Double,
        val ballSize: Double,
        val paddleWidth: Double,
        val paddleHeight: Double,
        val paddleMargin: Double,
        val winScore: Int
) : Notifier.Notification