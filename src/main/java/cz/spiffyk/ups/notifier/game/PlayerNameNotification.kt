package cz.spiffyk.ups.notifier.game

import cz.spiffyk.ups.notifier.Notifier

data class PlayerNameNotification(val playerId: Int, val name: String) : Notifier.Notification