package cz.spiffyk.ups.notifier

import javax.swing.JOptionPane

data class TextNotification(
        val text: String,
        val title: String? = null,
        val type: Type = Type.INFORMATION
) : Notifier.Notification {

    enum class Type(val swingType: Int) {
        PLAIN(JOptionPane.PLAIN_MESSAGE),
        INFORMATION(JOptionPane.INFORMATION_MESSAGE),
        WARNING(JOptionPane.WARNING_MESSAGE),
        ERROR(JOptionPane.ERROR_MESSAGE)
    }
}