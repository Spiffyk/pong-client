package cz.spiffyk.ups.notifier

import com.google.common.collect.MapMaker
import com.google.common.collect.Queues
import mu.KotlinLogging
import java.util.*

object Notifier {

    private val logger = KotlinLogging.logger {}

    /**
     * Notification queues. Notifications get pushed into these using [broadcast].
     */
    private val queues = Collections.synchronizedSet(
            Collections.newSetFromMap<Queue<Notification>>(MapMaker().weakKeys().makeMap()))

    /**
     * Sends a message to all registered queues.
     */
    @Synchronized
    fun broadcast(m: Notification) {
        for (queue in queues) {
            val success = queue.offer(m)
            if (!success) {
                logger.warn {
                    "Queue with identityHashCode ${System.identityHashCode(queue)} did not accept a message."
                }
            }
        }
    }

    /**
     * Registers the specified queue into the notifier.
     *
     * Queues are referenced weakly, omitting [unregister] will not cause a memory leak.
     */
    fun register(queue: Queue<Notification>) = queues.add(queue)

    /**
     * Unregisters the specified queue from the notifier.
     *
     * Queues are referenced weakly - it is not necessary to use this method in order to prevent memory leaks.
     */
    fun unregister(queue: Queue<Notification>) = queues.remove(queue)

    /**
     * Creates a recommended queue for use with the notifier and automatically registers it if not requested otherwise.
     *
     * @param register whether the queue should be automatically registered
     */
    fun queue(register: Boolean = true): Queue<Notification> {
        val queue = Queues.synchronizedQueue<Notifier.Notification>(LinkedList())
        if (register) {
            Notifier.register(queue)
        }
        return queue
    }

    /**
     * A notification that can be sent to listeners using [broadcast].
     */
    interface Notification

}