package cz.spiffyk.ups.notifier

data class ConnectionProblemsNotification(val desc: String) : Notifier.Notification