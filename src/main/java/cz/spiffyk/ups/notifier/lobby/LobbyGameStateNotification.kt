package cz.spiffyk.ups.notifier.lobby

import cz.spiffyk.ups.lobby.LobbyGame
import cz.spiffyk.ups.notifier.Notifier
import java.util.*

data class LobbyGameStateNotification(val uuid: UUID, val state: LobbyGame.State) : Notifier.Notification