package cz.spiffyk.ups.notifier.lobby

import cz.spiffyk.ups.notifier.Notifier
import java.util.*

data class LobbyGameNameNotification(val uuid: UUID, val name: String) : Notifier.Notification