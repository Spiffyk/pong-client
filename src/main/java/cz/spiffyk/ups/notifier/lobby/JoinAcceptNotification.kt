package cz.spiffyk.ups.notifier.lobby

import cz.spiffyk.ups.notifier.Notifier

data class JoinAcceptNotification(val playerNo: Int, val rejoinKey: Int) : Notifier.Notification