package cz.spiffyk.ups.notifier.lobby

import cz.spiffyk.ups.notifier.Notifier
import java.util.*

data class LobbyGameRemoveNotification(val uuid: UUID) : Notifier.Notification